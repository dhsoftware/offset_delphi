unit Language;

interface
  uses System.UITypes, System.Classes, System.Types, System.StrUtils,
  System.SysUtils, Dialogs, Forms, UConstants, UInterfaces, UInterfacesRecords,
  URecords, UVariables;

    procedure LanguageInit(filename: string);
    procedure ErrDlg (msgnos : array of integer);  overload;
    procedure ErrDlg (msgnos : array of integer; parameters : array of string); overload;
    procedure ErrDlg (msgno : integer);  overload;
    procedure ShowLbl (position, lblno : integer; parameters : array of string); overload;
    procedure ShowLbl (position, lblno : integer);  overload;
    procedure ShowLbl (position, lblno : integer; parameters : array of string; toggle : boolean); overload;
    procedure ShowLbl (position, lblno : integer; toggle : boolean); overload;
    procedure ShowMsg (msgno : integer; parameters : array of string);  overload;
    procedure ShowMsg (msgno : integer); overload;
    procedure ShowErr (msgno : integer; parameters : array of string); overload;
    procedure ShowErr (msgno : integer); overload;
    procedure ShowLvl (lblno : integer);
    function Msg (msgno : integer) : string;


implementation

  var
    Labels, Msgs : TStringList;


  function _SafeSplitLbl ( lblno : integer) : TStringDynArray;
  begin
    if lblno < Labels.Count then
      result := SplitString (Labels.Strings[lblno], '|')
    else
      result := SplitString ('***', '|');
  end;

  function _msg (msg : string; parameters : array of string) : string;
  var
    ndx, num : integer;
  begin
    result := msg;
    num := 0;
    ndx := Pos('$', result);
    while (ndx > 0) and (num <= high(parameters)) do begin
       Delete (result,ndx,1);
       insert (parameters[num], Result, ndx);
       ndx := Pos('$', result);
       num := num+1;
    end;
  end;

  function _getmsg (msgno : integer; parameters : array  of string) : string;
  var
    S : TStringDynArray;
  begin
    if msgno < Msgs.Count then begin
      S := SplitString (Msgs.Strings[msgno], '|');
      if high (S) > 0 then
        result := _msg (S[1], parameters)
      else if high(parameters) >= 0 then
        result := parameters[0]
      else result := '***';
    end
    else
      result := '***';
  end;

  procedure LanguageInit(filename: string);
  var
    temp : string[255];
    path : string;
  begin
    GetPath (temp, pathsup);
    path := temp + 'dhsoftware\';
    try
      Labels.LoadFromFile(path + filename + '.lbl');
      Msgs.LoadFromFile(path + filename + '.msg');
    except
      messagedlg ('Unable to open message files', mtError, [mbOK], 0);
   end;
  end;

  procedure ErrDlg (msgnos : array of integer);
  begin
    ErrDlg (msgnos, []);
  end;

  procedure ErrDlg (msgno : integer);
  begin
    ErrDlg ([msgno]);
  end;

  procedure ErrDlg (msgnos : array of integer; parameters : array of string);
  var
    msg : string;
    ndx : integer;
  begin
    msg := string(_getmsg (msgnos[0], []));
    ndx := 1;
    while ndx <= high(msgnos) do begin
      msg := msg + sLineBreak + string(_getmsg (msgnos[ndx], []));
      ndx := ndx + 1;
    end;

    messagedlg (_msg (msg, parameters), mtError, [mbOK], 0);
  end;

  procedure ShowLbl (position, lblno : integer; parameters : array of string);
  var
    S : TStringDynArray;
  begin
    S := _SafeSplitLbl (lblno);
    if high(S) >= 0 then lblset (position, shortstring(S[0]));
    if high(S) >= 1 then lblmsg (position, shortstring(_msg(S[1], parameters)));
  end;

  procedure ShowLbl (position, lblno : integer);
  begin
    ShowLbl (position, lblno, []);
  end;

  procedure ShowLbl (position, lblno : integer;
                     parameters : array of string;
                     toggle : boolean);
  var
    S : TStringDynArray;
  begin
    S := _SafeSplitLbl (lblno);
    if high(S) >= 0 then lblsett (position, shortstring(S[0]), toggle);
    if high(S) >= 1 then lblmsg (position, shortstring(_msg(S[1], parameters)));
  end;

  procedure ShowLbl (position, lblno : integer;
                     toggle : boolean);
  begin
    ShowLbl (position, lblno, [], toggle);
  end;

  procedure ShowLvl (lblno : integer);
  var
    S : TStringDynArray;
  begin
    S := _SafeSplitLbl (lblno);
    if high(S) >= 0 then wrtlvl (shortstring(S[0]));
  end;

  procedure ShowMsg (msgno : integer);
  begin
    ShowMsg (msgno,[]);
  end;

  procedure ShowMsg (msgno : integer; parameters : array of string);
  begin
    wrtmsg (shortstring(_getmsg(msgno, parameters)));
  end;

  procedure ShowErr (msgno : integer; parameters : array of string);
  begin
    wrterr (shortstring(_getmsg(msgno, parameters)));
  end;

  procedure ShowErr (msgno : integer);
  begin
    ShowErr (msgno, []);
  end;

  function Msg (msgno : integer) : string;
  begin
    result := _getmsg(msgno, []);
  end;


initialization
   Msgs := TStringList.Create;
   Labels := TStringList.Create;

finalization
   Msgs.Free;
   Labels.Free;
end.

