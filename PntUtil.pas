unit PntUtil;

interface

uses  UConstants, UInterfaces, UInterfacesRecords, URecords;

FUNCTION PntsEqual (p1, p2 : point; delta : real) : boolean;
FUNCTION  PntInPly (pnt : point;
										ply : array of point;
										npnt : integer) : integer;

implementation

const
   WeeNum = 1E-10;

FUNCTION PntsEqual (p1, p2 : point; delta : real) : boolean;
BEGIN
	result := (abs(p1.x-p2.x) <= delta) and (abs(p1.y-p2.y) <= delta);
END;


FUNCTION  PntInPly (pnt : point;  // note: default array is 0 based (whereas plyarr type is 1..maxply)
										ply : array of point;
										npnt : integer) : integer;
/// this function will return the following values:
///    1  if pnt is inside ply
///    0  if pnt is on the boundary of ply
///    -1 if pnt is outside ply
VAR
	i, j					: integer;
	ospnt					: point;	 { a point set up to be outside ply }
	minx, maxx,
	miny, maxy		: real;
	satisfactory	: boolean;
	intr					: point;
	xcount				: integer;
	trycount			: integer;
  angdif        : double;

BEGIN
	minx := ply[1].x;
	maxx := minx;
	miny := ply[1].y;
	maxy := miny;

	// check if on a boundary and also set up min & max x & y values
	for i := 0 to npnt-1 do begin
		if ply[i].x < minx then
			minx := ply[i].x;
		if ply[i].x > maxx then
			maxx := ply[i].x;
		if ply[i].y < miny then
			miny := ply[i].y;
		if ply[i].y > maxy then
			maxy := ply[i].y;

		j := i+1;
		if j > npnt-1 then
			j := 0;

    if pntsEqual (pnt, ply[i], WeeNum) then begin
      result := 0;
      exit;
    end;
		if dis_from_seg (ply[i], ply[j], pnt) < WeeNum then begin
      result := 0;
      exit;
	  end;
  end;

	// check if point is outside bounding rectangle (defined by min & max x & y values)
	if (pnt.x < minx) or (pnt.x > maxx) or (pnt.y < miny) or (pnt.y > maxy) then begin
		result := -1;
    exit;
	end;

	// set up ospnt to a point outside the bounding rectangle
	ospnt.x := minx-1.0;
	ospnt.y := miny + (maxy - miny)/2.0;

	// check how many times a line from ospnt to pnt crosses ply boundary
	// An odd number of times indicates that pnt is inside ply
	// An even number of times indicates that pnt is outside ply
	repeat
		satisfactory := true;
		trycount := 0;
		xcount := 0;
		for i := 0 to npnt-1 do begin
			j := i+1;
			if j > npnt-1 then
				j := 0;

			if intr_linlin (ospnt, pnt, ply[i], ply[j], intr, true) then begin
        angdif := angle(ospnt, pnt) - angle(ply[i], ply[j]);
        while angdif > Pi do angdif := angdif - Pi;
        while angdif < 0 do angdif := angdif + Pi;
        if (abs(angdif) < WeeNum) or (abs(angdif - Pi) < WeeNum) then
          satisfactory := false  // test line is parralel to polygon side
				else if PntsEqual (intr, ply[i], WeeNum) or PntsEqual (intr, ply[j], WeeNum) then begin
					// looks like our line goes though one of the points  ... result may not be reliable
					satisfactory := false;
					trycount := trycount+1;
					if PntsEqual (intr, ply[i], ABSZero) then
						xcount := xcount+1;

        end
				else
					xcount := xcount+1;
			end;
		end;
    if not satisfactory then
      ospnt.y := ospnt.y + (maxy - miny)/25; // move ospnt slightly for next iteration
	until satisfactory or (trycount > 15);

	if xcount mod 2 = 0 then
		result := -1
	else
		result := 1;
END;

end.
