unit AboutForm;

interface

uses
  UConstants, UDCLibrary, UInterfaces,
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, ShellAPI;

type
  TfAbout = class(TForm)
    Shape1: TShape;
    btnDismiss: TButton;
    Panel1: TPanel;
    lblwww: TLabel;
    lblCopyright: TLabel;
    lblOffset: TLabel;
    lblVersion: TLabel;
    btnInstructions: TButton;
    mCopyright: TMemo;
    lblContribute: TLabel;
    procedure lblwwwClick(Sender: TObject);
    procedure btnDismissClick(Sender: TObject);
    procedure btnInstructionsClick(Sender: TObject);
    procedure mCopyrightMouseEnter(Sender: TObject);
    procedure mCopyrightMouseLeave(Sender: TObject);
    procedure lblContributeClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Notice1, Notice2 : string;
  end;

var
  fAbout: TfAbout;

implementation

{$R *.dfm}

procedure TfAbout.btnDismissClick(Sender: TObject);
begin
  fAbout.close;
end;

procedure TfAbout.btnInstructionsClick(Sender: TObject);
var
  temp : shortstring;
  path : string;
begin
  UInterfaces.GetPath (temp, pathsup);
  path := temp + 'dhsoftware\OffsetInstructions.pdf';
  ShellExecute(self.WindowHandle,'open',PWideChar (path),nil,nil, SW_SHOWNORMAL);
end;

procedure TfAbout.lblContributeClick(Sender: TObject);
begin
  ShellExecute(self.WindowHandle,'open','www.dhsoftware.com.au/contribute.htm',nil,nil, SW_SHOWNORMAL);
end;

procedure TfAbout.lblwwwClick(Sender: TObject);
begin
  ShellExecute(self.WindowHandle,'open','www.dhsoftware.com.au',nil,nil, SW_SHOWNORMAL);
end;

procedure TfAbout.mCopyrightMouseEnter(Sender: TObject);
begin
  if Notice2.length > 0 then
    mCopyright.text := Notice2;
end;

procedure TfAbout.mCopyrightMouseLeave(Sender: TObject);
begin
  mCopyright.text := Notice1;
end;

end .
